	@EventHandler //イベントハンドラー
	public void onShot(PlayerInteractEvent event){
		Player player = event.getPlayer();  //イベントをおこしたプレイヤーを取得
		if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){ //ブロックまたは空気を右クリックした時
			if(player.getItemInHand() != null){  //手に持ってるのがnullじゃなったら
				if(player.getItemInHand().getType().equals(Material.BOW)){  //手に持ってるのが弓だったら
					Location loc = player.getLocation();  //プレイヤーの場所を取得
					while(true){  //繰り返し
						Block block = loc.clone().add(0, -1, 0).getBlock();  //下のブロックを取得
						loc = block.getLocation();  //locをblockの場所に設定
						if(block.getType() == Material.WOOL){  //下のブロックが羊毛だったら
							event.setCancelled(false);  //キャンセルをしない(一応)
							return;  //処理を終わる
						}
						if(block.getLocation().getBlockY() == 0){ //blockのYが０だったら
							break;  //whileから抜ける(これをやらないとチャットとかできなくなる)
						}
					}
					player.sendMessage(ChatColor.RED + "この場所では弓を打つことができません！");  //メッセージを送信
					event.setCancelled(true);   //キャンセルをする
				}
			}
		}
	}